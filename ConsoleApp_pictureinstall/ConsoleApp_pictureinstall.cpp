﻿// ConsoleApp_pictureinstall.cpp : このファイルには 'main' 関数が含まれています。プログラム実行の開始と終了がそこで行われます。
//


#include <iostream>
#include <string>

#include <iostream>
//#include <opencv2/core.hpp>
//#include <opencv2/highgui.hpp>
#include <opencv2/opencv.hpp>
#include "grubcut.h"



#ifdef _DEBUG
#pragma comment(lib, "opencv_world452d.lib")
#else
#pragma comment(lib, "opencv_world452.lib")
#endif

using namespace std;

int main()
{
	Grubcut gb;
	
	string name;
	cout << "Fileの完全Pathを入力して下さい" << endl;
	cin >> name;
	cout << name << endl;
	
	const char* windowName = "Image";
	
	//画像を格納するオブジェクトを宣言する、画像ファイルから画像データを読み込む
	cv::Mat img;
	//img = cv::imread("C:\\Users\\hosik\\Downloads\\imageopen\\imageopen\\birds_in_australia.jpg");
	img = cv::imread(name);
	
	/*
	// 行数
	std::cout << "rows:" << img.rows << std::endl;
	// 列数
	std::cout << "cols:" << img.cols << std::endl;
	// 次元数
	std::cout << "dims:" << img.dims << std::endl;
	// サイズ（2次元の場合）
	std::cout << "size[]:" << img.size().width << "," << img.size().height << std::endl;
	// ビット深度ID
	std::cout << "depth (ID):" << img.depth() << "(=" << CV_64F << ")" << std::endl;
	// チャンネル数
	std::cout << "channels:" << img.channels() << std::endl;
	// （複数チャンネルから成る）1要素のサイズ [バイト単位]
	*/

	if (img.empty() == true) {
		// 画像データが読み込めなかったときは終了する
		return 0;
	}

	Mat dst;
	dst = gb.BR_Grubcut(img);


	//ウィンドウに画像を表示する
	cv::imshow(windowName, dst);

	cv::waitKey(0);
	
	return 0;
}

