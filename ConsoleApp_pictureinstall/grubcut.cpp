#include "grubcut.h"
#include <iostream>
#include <opencv2/opencv.hpp>
#include<opencv2/core/core.hpp>
#include<opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc.hpp>

using namespace cv;
using namespace std;

cv::Point minPoint(vector<cv::Point> contours) {
    double minx = contours.at(0).x;
    double miny = contours.at(0).y;
    for (int i = 1; i < contours.size(); i++) {
        if (minx > contours.at(i).x) {
            minx = contours.at(i).x;
        }
        if (miny > contours.at(i).y) {
            miny = contours.at(i).y;
        }
    }
    return cv::Point(minx, miny);
}
//最大座標を求める
cv::Point maxPoint(vector<cv::Point> contours) {
    double maxx = contours.at(0).x;
    double maxy = contours.at(0).y;
    for (int i = 1; i < contours.size(); i++) {
        if (maxx < contours.at(i).x) {
            maxx = contours.at(i).x;
        }
        if (maxy < contours.at(i).y) {
            maxy = contours.at(i).y;
        }
    }
    return cv::Point(maxx, maxy);
}


Mat Grubcut::BR_FindCoutors(Mat src) {
    Mat src_img = src.clone();

    //パラメータ
    int BLUR = 21;
    int CANNY_THRESH_1 = 90;
    int CANNY_THRESH_2 = 200;
    int MASK_DILATE_ITER = 10;
    int MASK_ERODE_ITER = 10;
    int MASK_COLOR = (0.0, 0.0, 1.0); // In BGR format

    //gray変換　RGB > GRAY
    Mat gray;
    cvtColor(src_img, gray, COLOR_BGR2GRAY);


    //canny二値化
    Mat edges;
    Canny(gray, edges, CANNY_THRESH_1, CANNY_THRESH_2);
    dilate(edges, edges, Mat());
    erode(edges, edges, Mat());

    //エッジ検出
    vector< vector<cv::Point> > contours;
    vector<cv::Vec4i> hierarchy;
    findContours(edges, contours, hierarchy, RETR_LIST, CHAIN_APPROX_NONE);

    Mat mask = Mat::zeros(edges.size(), CV_8UC3);

    //最小座標を求める
    for (int i = 0; i < contours.size(); i++) {
        cv::Point minP = minPoint(contours.at(i));
        cv::Point maxP = maxPoint(contours.at(i));
        cv::Rect rect(minP, maxP);
        //矩形を描く
        cv::rectangle(src_img, rect, cv::Scalar(0, 255, 0), 2, 8);
        fillConvexPoly(mask, contours.at(i), cv::Scalar(255, 255, 255), 16, 0);
    }


    //ノイズ除去
    dilate(mask, mask, Mat(), Point(-1, -1), 10);
    erode(mask, mask, Mat(), Point(-1, -1), 10);
    GaussianBlur(mask, mask, Size(BLUR, BLUR), 0);

    //出力画像
    Mat dst;
    //マスク画像合成
    bitwise_and(src, mask, dst);

    return dst;
}

Rect2i rectangle_value;

void  mouse_callback(int event, int x, int y, int flags, void* userdata)
{
    int x1, y1, x2, y2;

    bool* isClick = static_cast<bool*>(userdata);
    if (event == EVENT_LBUTTONDOWN) {
        *isClick = true;
        cout << "Draw rectangle\n"
            << " start position (x, y) : " << x << ", " << y << endl;
        rectangle_value = Rect2i(x, y, 0, 0);
        x1, y1 = x, y1;

    }
    if (event == EVENT_LBUTTONUP) {
        *isClick = false;
        cout << " end   position (x, y) : " << x << ", " << y << endl;
        rectangle_value.width = x - rectangle_value.x;
        rectangle_value.height = y - rectangle_value.y;
        x2, y2 = x, y;
    }
    if (event == EVENT_MOUSEMOVE) {
        if (*isClick) {
            rectangle_value.width = x - rectangle_value.x;
            rectangle_value.height = y - rectangle_value.y;
        }
    }

    //return x1, y1, x2, y2;
}



//マウス入力用のパラメータ
struct mouseParam {
    int x;
    int y;
    int event;
    int flags;
};
//コールバック関数
void CallBackFunc(int eventType, int x, int y, int flags, void* userdata)
{
    mouseParam* ptr = static_cast<mouseParam*> (userdata);

    ptr->x = x;
    ptr->y = y;
    ptr->event = eventType;
    ptr->flags = flags;
}

/*
Rect2i rectangle_value;
void  mouse_callback(int event, int x, int y, int flags, void* userdata)
{
    if (event == EVENT_LBUTTONDOWN) {
        cout << x << "," << y << endl;
    }
}
*/

Mat Grubcut::BR_Grubcut(Mat src) {
    Mat src_img = src.clone();

    Mat result; // segmentation result (4 possible values)
    Mat bgModel, fgModel; // the models (internally used)

    Mat draw_img = src_img.clone();


    mouseParam mouseEvent;

    //表示するウィンドウ名

    bool iisClick = false;
    int kkey;
    string flag;

    while (1) {
        bool isClick = false;
        int key;

        key = 0;
        cv::String showing_name = "input";
        int x, y, x2, y2;


        imshow(showing_name, draw_img);
        cout << "第1点を指定してください" << endl;

        setMouseCallback(showing_name, CallBackFunc, &mouseEvent);
        while (1) {
            cv::waitKey(20);
            //左クリックがあったら表示
            if (mouseEvent.event == cv::EVENT_LBUTTONDOWN) {

                //クリック後のマウスの座標を出力
                std::cout << "x :" << mouseEvent.x << ", y : " << mouseEvent.y << std::endl;
                x = mouseEvent.x;
                y = mouseEvent.y;
                break;
            }

        }
        cout << x << y << endl;
        waitKey(0);

        cout << "第2点を指定してください" << endl;
        //cout << " 指定後 Enter 入力" << endl;
        setMouseCallback(showing_name, CallBackFunc, &mouseEvent);
        while (1) {
            cv::waitKey(20);
            //左クリックがあったら表示
            if (mouseEvent.event == cv::EVENT_LBUTTONDOWN) {

                //クリック後のマウスの座標を出力
                std::cout << "x2 : " << mouseEvent.x << " , y2 : " << mouseEvent.y << std::endl;
                x2 = mouseEvent.x;
                y2 = mouseEvent.y;
                break;
            }
        }



        destroyAllWindows();
        cv::rectangle(draw_img, Point(x, y), Point(x2, y2), Scalar(255, 0, 0), 3, 8);
        imshow("ttt", draw_img);
        cout << x << "," << y << "," << x2 << "," << y2 << endl;

        Rect rectangle(x, y, x2 - x, y2 - y);
        // GrabCut segmentation
        grabCut(src_img,    // input image
            result,   // segmentation result
            rectangle,// rectangle containing foregroundAWdDwadwadaWdA
            bgModel, fgModel, // models
            1,        // number of iterations
            cv::GC_INIT_WITH_RECT); // use rectangle
                                        // Get the pixels marked as likely foreground

        compare(result, cv::GC_PR_FGD, result, cv::CMP_EQ);
        // Generate output image

        Mat foreground(src_img.size(), CV_8UC3, cv::Scalar(0, 0, 0));
        src_img.copyTo(foreground, result); // bg pixels not copied

        // draw rectangle on original image

        cv::rectangle(draw_img, rectangle, cv::Scalar(0, 255, 255), 1);

        imshow("temp", result);
        waitKey(0);
        destroyAllWindows();

        cout << "数値を入力" << endl;
        cout << "repeat : a, redo : b, end : other key" << endl;

        flag = getchar();
        cout << flag << endl;

        if (flag == "a") {
            src_img = src;
        }
        else if (flag == "b") {
            src_img = foreground;
        }
        else {
            break;
        }

    }
    
    Mat dst;
    dst = src_img;

    return dst;

}


Mat Grubcut::BR_otsu(Mat src) {
    Mat src_img = src;

    cvtColor(src_img, src_img, COLOR_BGR2GRAY);

    Mat mask;
    threshold(src_img, mask, 0, 255, THRESH_BINARY | THRESH_OTSU);

    return mask;

}

