#include<opencv2/opencv.hpp>
#include<opencv2/core/core.hpp>
#include<opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc.hpp>
#pragma once

using namespace cv;
using namespace std;

//grubcut.h
class Grubcut {
public:
    Mat BR_FindCoutors(Mat src);
    Mat BR_Grubcut(Mat src);
    Mat BR_otsu(Mat src);

};
