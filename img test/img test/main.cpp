//#include"stdafx.h"
//#pragma comment(lib,"opencv_world341d.lib")
#include <iostream>
#include<opencv2/opencv.hpp>
#include<opencv2/core/core.hpp>
#include<opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc.hpp>

using namespace cv;
using namespace std;



cv::Point minPoint(vector<cv::Point> contours) {
    double minx = contours.at(0).x;
    double miny = contours.at(0).y;
    for (int i = 1; i < contours.size(); i++) {
        if (minx > contours.at(i).x) {
            minx = contours.at(i).x;
        }
        if (miny > contours.at(i).y) {
            miny = contours.at(i).y;
        }
    }
    return cv::Point(minx, miny);
}
//最大座標を求める
cv::Point maxPoint(vector<cv::Point> contours) {
    double maxx = contours.at(0).x;
    double maxy = contours.at(0).y;
    for (int i = 1; i < contours.size(); i++) {
        if (maxx < contours.at(i).x) {
            maxx = contours.at(i).x;
        }
        if (maxy < contours.at(i).y) {
            maxy = contours.at(i).y;
        }
    }
    return cv::Point(maxx, maxy);
}

int main()
{
    // Open another image
    Mat image;
    image = cv::imread("C:\\Users\\gofor\\source\\repos\\img subtraction\\test.bmp");

    if (!image.data) // Check for invalid input
    {
        cout << "Could not open or find the image" << endl;
        return -1;
    }
    int width = image.rows;
    int height = image.cols;

    cout << width << height << endl;

    /*
    // define bounding rectangle
    //int border = 100;
    //int border2 = border + border;
    //cv::Rect rectangle(100, 100, image.cols - border2, image.rows - border2);
    cv::Rect rectangle(20, 20, 450, 450);

    cv::Mat result; // segmentation result (4 possible values)
    cv::Mat bgModel, fgModel; // the models (internally used)

                              // GrabCut segmentation
    cv::grabCut(image,    // input image
        result,   // segmentation result
        rectangle,// rectangle containing foreground 
        bgModel, fgModel, // models
        1,        // number of iterations
        cv::GC_INIT_WITH_RECT); // use rectangle
                                // Get the pixels marked as likely foreground
    cv::compare(result, cv::GC_PR_FGD, result, cv::CMP_EQ);
    // Generate output image
    cv::Mat foreground(image.size(), CV_8UC3, cv::Scalar(0, 255, 255));
    image.copyTo(foreground, result); // bg pixels not copied

                                      // draw rectangle on original image
    cv::rectangle(image, rectangle, cv::Scalar(0, 255, 255), 1);
    cv::namedWindow("Image");
    cv::imshow("Image", image);
    */

    //パラメータ
    int BLUR = 21;
    int CANNY_THRESH_1 = 100;
    int CANNY_THRESH_2 = 150;
    int MASK_DILATE_ITER = 10;
    int MASK_ERODE_ITER = 10;
    int MASK_COLOR = (0.0, 0.0, 1.0); // In BGR format

    //gray変換　RGB > GRAY
    Mat gray;
    cvtColor(image, gray, COLOR_BGR2GRAY);


    Mat edges;
    Canny(gray, edges, CANNY_THRESH_1, CANNY_THRESH_2);
    dilate(edges, edges, Mat());
    erode(edges, edges, Mat());

    vector< vector<cv::Point> > contours;
    vector<cv::Vec4i> hierarchy;
    findContours(edges, contours, hierarchy, RETR_LIST, CHAIN_APPROX_NONE);

    cout << contours.size() << endl;

    Mat mask = Mat::zeros(edges.size(), CV_32FC3);
    //contours.size()
    for (int i = 0; i < contours.size(); i++) {
        cv::Point minP = minPoint(contours.at(i));
        cv::Point maxP = maxPoint(contours.at(i));
        cv::Rect rect(minP, maxP);
        //矩形を描く
        cv::rectangle(image, rect, cv::Scalar(0, 255, 0), 2, 8);
        fillConvexPoly(mask, contours.at(i), cv::Scalar(1.0, 1.0, 1.0), 16, 0);
    }
    //最小座標を求める

    //cout << contours.at(294) << endl;
    dilate(mask, mask, Mat(), Point(-1, -1), 10);
    erode(mask, mask, Mat(), Point(-1, -1), 10);
    GaussianBlur(mask, mask, Size(BLUR, BLUR), 0);

    //vector<Mat> tmp;
    //tmp[0] = mask;
    //tmp[1] = mask;
    //tmp[2] = mask;

    Mat dst;
    //merge(tmp, dst);
    cout << mask.channels() << endl;
    //cvtColor(mask, dst, COLOR_GRAY2BGR);

    // display result
    cv::namedWindow("Segmented Image");
    //cv::imshow("Segmented Image", foreground);

    cv::imshow("Segmented Image", image);
    cv::imshow("edges", dst);

    waitKey();
    return 0;
}



///////////////////////////////////////////////////////////////////////////////////////
/*
#include<cstdlib>
#include<string>
#include<stdexcept>
#include<opencv2\opencv.hpp>


using namespace std;
using namespace cv;

std::string get_environment_variable(const char* environment_name) {
    size_t buf;
    if (getenv_s(&buf, NULL, 0, environment_name)) throw runtime_error("read error");
    if (buf == 0) throw runtime_error("not such environment variable");
    string buffer;
    buffer.resize(buf + 1);
    getenv_s(&buf, &buffer[0], buf, environment_name);
    buffer.resize(std::strlen(buffer.c_str()));
    return buffer;
}

int main(void) {

    cout << "Hello World" << endl;

    //string dir = get_environment_variable("OPENCV_DIR");
    //cout << dir << endl;

    string tmp = "C:\\Users\\gofor\\source\\repos\\img subtraction\\test.bmp";

    Mat img = cv::imread(tmp);
    if (img.empty()) return -1;
    imshow("View", img);
    waitKey();

    return 0;
}
*/